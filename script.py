import subprocess

# install virtual env with module venv
subprocess.run(["py", "-m", "venv venv"])

# activate virtual env
subprocess.run(["venv\Scripts\activate"])

# install packages into requirements
subprocess.run(["pip", "install", "-r", "requirements.txt"])