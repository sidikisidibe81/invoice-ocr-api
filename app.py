from flask import (
    Flask,
    request,
    jsonify,
)
import logging
import sys
import os
from helpers import helpers
from helpers import aws_helpers
from services import services
app = Flask(__name__)
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.ERROR)
filename = ''

@app.errorhandler(404)
def not_found(error):
    resp = jsonify({u"status": 404, u"message": u"Resource not found"})
    resp.status_code = 404
    return resp


@app.route("/")
def api_root():
    resp = jsonify(
        {u"statusCode": 200, u"status": "Up", u"message": u"Welcome to invoice ocr api"}
    )
    resp.status_code = 200
    return resp


@app.route("/invoice/upload", methods=["POST"])
def upload():
    if request.method == "POST":
        data = request.get_json(force=True)
        url = data["url"]
        lang = data["languages"]

        filename = os.path.basename(url or "")

        if helpers.allowed_file(filename):
            fileLocation = helpers.getFileLocation(url)
            #result = invoice2data(url, lang, fileLocation)
            result = aws_helpers.upload_to_aws(url, filename)
        else:
            return {"error": "Invalid file format"}, 400
        print("######## invoice ####### \n")
        print(result)
        print("######## invoice end ####### \n")
        return jsonify(result)


@app.route("/invoice/uploadStatus", methods=["GET"])
def getStatus():
    if request.method == "GET":
        data = request.get_json(force=True)
        x_key = data["filename"]
        return services.getJobStatus(x_key)

@app.route("/invoice/extract", methods=["GET"])
def extract():
    if request.method == "GET":
        data = request.get_json(force=True)
        x_key = data["filename"]
        return services.read_file(x_key)

if __name__ == "__main__":
    app.run(debug=True)
