import urllib3
import json
import re
from unidecode import unidecode

def getJobStatus(xKey):
    http = urllib3.PoolManager()
    
    r = http.request(
        'GET',
        'https://3xgzf049ag.execute-api.us-east-2.amazonaws.com/default/getJobStatus',
        headers={
            'x-key':xKey,
            'x-api-key':'SfvHzSscOLasuZjUtjObp2ZPvOHma5zv2ZXrk8TW',
            'Content-Type': 'application/json'
        }
    )

    return json.loads(r.data.decode('utf-8'))


def parse_string(value):
    """
    Parse la chaine en supprimant \n \t \r
    
    :param value : la valeur de la chaine à parser
    :type value : String
    
    :return : la chaine parsée
    :rtype : String
    """

    chaine = value.replace('\n', '')
    chaine = " ".join(chaine.split()).rstrip(',').strip(',').strip()
    return chaine

def formatInvoice(invoice):
    regex = re.compile(r"[\n\r\t]")
    optimized_str = regex.sub(" ", invoice)
    optimized_str = re.sub("  ", " ", optimized_str)
    optimized_str = unidecode(optimized_str)
    return optimized_str

def read_file(filelink):
    with open('ParsedResult(2).txt') as f:
        lines = f.read()
    invoice = formatInvoice(lines).strip()
    return invoice
