import validators
import yaml
import os
import re
from unidecode import unidecode


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1] in set(
        ["png", "jpg", "jpeg", "gif", "tif", "tiff", "pdf"]
    )


def getFileLocation(url):
    valid = validators.url(url)
    if valid == True:
        return "remote"
    else:
        return "local"


def formatInvoice(invoice):
    regex = re.compile(r"[\n\r\t]")
    optimized_str = regex.sub(" ", invoice)
    optimized_str = re.sub("  ", "", optimized_str)
    optimized_str = unidecode(optimized_str)
    return optimized_str


def loadTemplate():
    with open(os.path.join("./templates", "invoice.yml")) as file:
        invoiceTemplate = yaml.load(file, Loader=yaml.FullLoader)
    return invoiceTemplate


def parse_date(value):
    """Parses date and returns date after parsing"""
    res = dateparser.parse(
        value,
        date_formats="%d/%m/%Y",
        languages="fr",
    )
    return res


### functions extract
def extractNumFact(num):
    if re.search(":", num):
        tabNum = num.split(":")
        return tabNum[len(tabNum) - 1]
    elif re.search(".", num):
        tabNum = num.split(".")
        return tabNum[len(tabNum) - 1]
    else:
        tab = num.split(" ")
        if len(tab) == 1:
            return tab[0]
        else:
            return tab[2]


def extractDate(date):
    if re.search(":", date):
        tabDate = date.split(":")
        return tabDate[len(tabDate) - 1]
    elif re.search(".", date):
        tabDate = date.split(".")
        return tabDate[len(tabDate) - 1]
    else:
        tab = date.split(" ")
        if len(tab) == 1:
            return tab[0]
        elif len(tab) == 2:
            return tab[1]
        else:
            return date


def getNumFact(invoice, template):
    ouput = ""
    num_fac = template["num"]
    for num, value in enumerate(num_fac, start=1):
        if num == 1:
            text = re.search(r"{}".format(value), invoice)
        else:
            text = re.search(value, invoice)
        if text:
            ouput = text.group()
            break
    return extractNumFact(ouput)


def getDateFact(invoice, template):
    output = ""
    dateFormat = template["date"]
    for value in dateFormat:
        date = re.search(value, invoice)
        if date:
            output = date.group()
            break
    return extractDate(output)
