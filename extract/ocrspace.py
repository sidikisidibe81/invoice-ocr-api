# -*- coding: utf-8 -*-
import string


def extract(invoiceUrl, language, fileLocation):
    """Wraps OCR Space API.

    Parameters
    ----------
    path : str
        path of electronic invoice in JPG or PNG format

    Returns
    -------
    extracted_str : str
        returns extracted text from image in JPG or PNG format

    """
    import requests
    import json
    import os
    import sys

    if "OCRSPACE_SUBSCRIPTION_KEY" in os.environ:
        api_key = os.environ["OCRSPACE_SUBSCRIPTION_KEY"]
    else:
        print(
            "\nSet the OCRSPACE_SUBSCRIPTION_KEY environment variable.\n**Restart your shell or IDE for changes to take effect.**"
        )
        sys.exit()
    overlay = False

    payload = {
        "isOverlayRequired": overlay,
        "apikey": api_key,
        "language": language,
        "detectOrientation": True,
        "isCreateSearchablePdf": True,
        "isSearchablePdfHideTextLayer": False,
        "isTable": True,
        "scale": True,
        "OCREngine": 2,
        "isTable": True,
    }
    if fileLocation == "remote":
        payload["url"] = invoiceUrl
        r = requests.post(
            "https://api.ocr.space/parse/image",
            data=payload,
        )
    elif fileLocation == "local":
        with open(invoiceUrl, "rb") as f:
            r = requests.post(
                "https://api.ocr.space/parse/image",
                files={invoiceUrl: f},
                data=payload,
            )
    extracted_str = ""
    d = json.loads(r.content.decode())
    extracted_str = d["ParsedResults"][0]["ParsedText"]

    return extracted_str
