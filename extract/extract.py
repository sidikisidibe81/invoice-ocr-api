from .ocrspace import extract
from helpers import helpers


def invoice2data(invoiceUrl, language, fileLocation):
    invoice = extract(invoiceUrl, language, fileLocation)
    invoiceFormat = helpers.formatInvoice(invoice)
    invoiceTemplate = helpers.loadTemplate()
    num = helpers.getNumFact(invoiceFormat, invoiceTemplate)
    print("\n>>>>>>>>>>> numero facture : ", num)
    date = helpers.getDateFact(invoiceFormat, invoiceTemplate)
    print("\n>>>>>>>>>>> date facture : ", date)
    return invoiceFormat